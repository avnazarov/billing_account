from src.db_service import Account

def test_db_service_add_account(db_service, session):
    db_service.create_account({"name": "test1"})

    account = session.query(Account).first()
    
    assert account.name == "test1"
    assert account.id == 1
    assert account.created_date
