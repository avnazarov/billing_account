def test_account(account):
    assert account.name == "test1"
    assert account.value == 0

def test_deposite(deposite):
    assert deposite.value == 200
    assert deposite.operation == "add"
