from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import declarative_base
from sqlalchemy.sql import func


Base = declarative_base()

class Account(Base):
    """
    class describing the account
    """
    __tablename__ = "account"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, nullable=False)
    balance = Column(Integer, default=0)
    created_date = Column(DateTime, server_default=func.now())


class Operation(Base):
    """
    class describing the operation
    """
    
    __tablename__ = "operation"

    id = Column(Integer, primary_key=True)
    value = Column(Integer, default=0)
    name = Column(String)
    date = Column(DateTime, server_default=func.now())
    reason = Column(String)
    account_id = Column(Integer, ForeignKey("account.id"))
